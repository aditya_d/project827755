# -*- coding: utf-8 -*-
"""
===================================
DBSCAN clustering algorithm, SCIKIT-LEARN
===================================

Finds core samples of high density and expands clusters from them.
# . Black points correspond to outliers
"""
print(__doc__)

import numpy as np
import pandas as pd


from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler



#
# #############################################################################
# Importing coordinates into a csv file. CSV is easier to read.
f = open("./dbscan_PERTH.csv","rb")
centers = pd.read_csv(f)
X, labels_true = make_blobs(n_samples=100,center_box=(1, 10.0) ,centers=centers,shuffle=True, cluster_std=0.4,
                            random_state=0)

X = StandardScaler().fit_transform(X)

# #############################################################################
# Compute DBSCAN
db = DBSCAN(eps=0.2, min_samples=4).fit(X) # eps = degreees here
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels))

# #############################################################################
# Plot result using matplotlib
import matplotlib.pyplot as plt

# Black removed and is used for noise instead.
unique_labels = set(labels)



colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]

    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=14)

    xy = X[class_member_mask & ~core_samples_mask]

    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=6)


 # Plot the training points
plt.scatter(X[:, 0], X[:, 1])

plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.savefig("DBSCAN-PERTH NOISE.png")  # saving the file in local disk
plt.gray()

plt.show()
