
from json import loads

with open('Tweets.txt', encoding='utf-8-sig') as my_tweets:
    tweet_data = my_tweets.read()
    tweet_loader = loads(tweet_data)['rows']

    tweet_list =[] # storing in a database
    for v in tweet_loader:
        value = v['value']
        coor = ','.join([str(k) for k in value['geometry']['coordinates']][::-1])
        #print(value['properties']['location'])
        print(coor)
        tweet_list.append(coor)

    print(tweet_list)
#write the coordinates in a file